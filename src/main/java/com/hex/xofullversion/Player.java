/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hex.xofullversion;

import java.io.Serializable;

/**
 *
 * @author Code.Addict
 */
public class Player implements Serializable {

	private int side, win, lose, draw;

	Player(int sideIn) {
		this.side = sideIn;
		this.win = 0;
		this.lose = 0;
		this.draw = 0;
	}

	void drawAdd() {
		this.draw++;
	}

	void winAdd() {
		this.win++;
	}

	void loseAdd() {
		this.lose++;
	}

	@Override
	public String toString() {
		
		return "Side" + this.side + " Win [ " + this.win + " ] | Draw [ " + this.draw + " ] | Lose [ " + this.lose + " ]";
	}

}
