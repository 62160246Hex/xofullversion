/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hex.xofullversion;

/**
 *
 * @author Code.Addict
 */
public class Table {

	private int turn, round, winner;
	private boolean isEnd;
	private int[] data;
	private Player xSide, oSide;

	Table(Player xSide, Player oSide) {

		this.xSide = xSide;
		this.oSide = oSide;
		this.data = new int[]{
			0, 0, 0,
			0, 0, 0,
			0, 0, 0
		};

		this.round = 1;
		this.winner = -1;
		this.turn = this.randomTurn();
	}

	int randomTurn() {
		return Math.floor(Math.random() * 10) < 5 ? 1 : 2;
	}

	int getTurn() {
		return this.turn;
	}

	int getRound() {
		return this.round;
	}

	boolean checkIssueInsert(int indexSelected) {
		return this.data[indexSelected] == 0;
	}

	int getWinner() {
		return this.winner;
	}

	void setWinner() {
		this.winner = (this.data[0] == this.turn && this.data[1] == this.turn && this.data[2] == this.turn) ? this.turn
			: (this.data[3] == this.turn && this.data[4] == this.turn && this.data[5] == this.turn) ? this.turn
				: (this.data[6] == this.turn && this.data[7] == this.turn && this.data[8] == this.turn) ? this.turn
					: (this.data[0] == this.turn && this.data[3] == this.turn && this.data[6] == this.turn) ? this.turn
						: (this.data[1] == this.turn && this.data[4] == this.turn && this.data[7] == this.turn) ? this.turn
							: (this.data[2] == this.turn && this.data[5] == this.turn && this.data[8] == this.turn) ? this.turn
								: (this.data[0] == this.turn && this.data[4] == this.turn && this.data[8] == this.turn) ? this.turn
									: (this.data[2] == this.turn && this.data[4] == this.turn && this.data[6] == this.turn) ? this.turn : this.round > 8 ? 3 : -1;
	}

	void releaseResult() {
		if (this.winner == 3) {
			this.xSide.drawAdd();
			this.oSide.drawAdd();
		} else if (this.winner == 2) {
			this.oSide.winAdd();
			this.xSide.loseAdd();
		} else if (this.winner == 1) {
			this.xSide.winAdd();
			this.oSide.loseAdd();
		}
	}

	void insertIndex(int indexSelected) {
		this.data[indexSelected] = this.getTurn();
	}

	void switchPlayer() {
		this.turn = this.turn == 1 ? 2 : 1;
		this.round++;
	}

}
